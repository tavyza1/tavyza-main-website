function resizeHeader() {
    if (window.innerWidth < 470) {
        document.querySelector("header").style["border-style"] =
            "none none solid none";
        document.querySelector("header").style["border-radius"] = "0";
        document.querySelector("header").style.margin = "0";
    }
    if (window.innerWidth < 360) {
        document.querySelector("#title > img").style.display = "none";
        document.querySelector("main").style.margin = "0";
    }
}
document.addEventListener("DOMContentLoaded", () => {
    fetch("/header.html")
        .then((response) => response.text())
        .then((data) => (document.querySelector("header").outerHTML = data))
        .then(resizeHeader);
});
window.addEventListener("resize", resizeHeader, true);
