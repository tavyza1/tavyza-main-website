#!/usr/bin/env sh
# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep .2; done

polybar --reload -c ~/.config/polybar/config.ini main &
polybar --reload -c ~/.config/polybar/config-bottom.ini main &

